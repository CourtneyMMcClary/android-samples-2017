package com.example.a003013540.samples2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

public class ImageSpinnerActivity extends AppCompatActivity {

    public static final String TAG = "ImageSpinnerActivity";

    Spinner spinner;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_spinner);

        imageView = (ImageView)findViewById(R.id.imageView);
        spinner = (Spinner)findViewById(R.id.spinner);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = spinner.getSelectedItem().toString();
                changeImage(selectedItem);
                Log.d(TAG, "Just pick a new image!");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }//end of onCreate()

    private void changeImage(String selected){
        int resourceId = 0;

        switch(selected){
            case "Desert":
                resourceId = R.drawable.desert;
                break;

            case "Jellyfish":
                resourceId = R.drawable.jellyfish;
                break;

            case "Koala":
                resourceId = R.drawable.koala;
                break;

            case "Lighthouse":
                resourceId = R.drawable.lighthouse;
                break;

            case "Penguins":
                resourceId = R.drawable.penguins;
                break;

            case "Tulips":
                resourceId = R.drawable.tulips;
                break;

        }//end of switch case

        imageView.setImageResource(resourceId);
    }

}
