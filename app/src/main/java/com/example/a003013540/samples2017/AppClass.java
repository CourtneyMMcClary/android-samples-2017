package com.example.a003013540.samples2017;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.example.a003013540.samples2017.dataaccess.UserDataAccess;
import com.example.a003013540.samples2017.models.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by 003013540 on 10/23/2017.
 */

public class AppClass extends Application {

    public static final String TAG ="AppClass";
//    public String someGlobalVariable = "HELLO";
   public static ArrayList<User> users = new ArrayList();

    SQLiteHelperSubClass dbHelper;
    UserDataAccess da;

    @Override
    public void onCreate() {
        super.onCreate();

        dbHelper = new SQLiteHelperSubClass(this);
        da = new UserDataAccess(dbHelper);

        //we did this just to create some initial users on file
      //  users.add(new User(1,"Bob", "bob@bob.com", User.Music.COUNTRY, true));
//        users.add(new User(2,"Sally", "sally@sally.com", User.Music.JAZZ, true));
//        users.add(new User(3,"Betty", "betty@betty.com", User.Music.COUNTRY, true));
//        writeUsersToFile(users, USERS_FILE);
       // users = readUsersFromFile(USERS_FILE);

        //Toast.makeText(this, users.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public void writeUsersToDb(ArrayList<User>user, String filePath){
        try{
            FileOutputStream fos = openFileOutput(filePath, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            //oos.writeObject(users);
            oos.close();
            fos.close();
        }catch(FileNotFoundException e){
            Log.e(TAG,"File Not Found");
        }catch(IOException e){
            Log.e(TAG,"IO ERROR");
        }catch(Exception e){
            Log.e(TAG, "General Error");
        }
    }

    public ArrayList<User> readUsersFromDb(String filePath){

        ArrayList<User>users = new ArrayList<>();

        try{
            FileInputStream is = openFileInput(filePath);
            ObjectInputStream ois = new ObjectInputStream(is);
            users = (ArrayList<User>)ois.readObject();
            ois.close();
            is.close();
        }catch(FileNotFoundException e){
            Log.e(TAG,"File Not Found");
        }catch(IOException e){
            Log.e(TAG,"IO ERROR");
        }catch(Exception e){
            Log.e(TAG, "General Error");
        }

        return users;
    }

    public static User getUserById(long id){
        for(User u : users){
            if(id == u.getId()){
                return u;
            }
        }

        return null;
    }
}
