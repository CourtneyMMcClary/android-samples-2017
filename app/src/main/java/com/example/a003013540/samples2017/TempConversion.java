package com.example.a003013540.samples2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class TempConversion extends AppCompatActivity {

    EditText temp;
    RadioButton toC;
    RadioButton toF;



    public static double celsiusToFarenheit(double c){
        return 32+c*9/5;

    }
    public static double farenheitToCelsius(double f){
        return (f-32)*5/9;

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_conversion);


        temp = (EditText)findViewById(R.id.tempEdit);
        toC = (RadioButton)findViewById(R.id.rdoFahrtoCel);
        toF = (RadioButton)findViewById(R.id.rdoFromCelToFahr);
    }

    public void convert(View view){
        double value = new Double(temp.getText().toString());
        if(toC.isChecked()){
            value = TempConversion.farenheitToCelsius(value);
        }else{
            value = TempConversion.celsiusToFarenheit(value);
        }
        temp.setText(new Double(value).toString());
    }
}
