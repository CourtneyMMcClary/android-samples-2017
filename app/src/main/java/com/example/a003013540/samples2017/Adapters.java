package com.example.a003013540.samples2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a003013540.samples2017.models.User;

import java.util.ArrayList;

import static com.example.a003013540.samples2017.AppClass.users;

public class Adapters extends AppCompatActivity {

    public static final String TAG = "Adapters";
    ListView listView;

    String[] names = {"Bob", "Sally", "Betty"};
    ArrayList<User> user = new ArrayList<User>();
    AppClass app;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapters);

        app = (AppClass)getApplication();
       // Toast.makeText(this, app.someGlobalVariable, Toast.LENGTH_SHORT).show();

        listView = (ListView) findViewById(R.id.listView);

        user.add(new User(1,"Bob", "bob@bob.com", User.Music.COUNTRY, true));
        user.add(new User(2,"Sally", "sally@sally.com", User.Music.JAZZ, true));
        user.add(new User(3,"Betty", "betty@betty.com", User.Music.COUNTRY, true));

        example(); //uses array of strings as data source

        //example2(); //uses arraylist of Users as the data source

        //example3(); //uses custom view for each item in the user arraylist


    }

    public void example() {

        ArrayList<String> userStrings = new ArrayList<>();
        for(User u : users){
            userStrings.add(u.getFirstName() + " " + u.getFavoriteMusic());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, userStrings);
        listView.setAdapter(adapter);

        //easy solution to subclassing the ArrayAdapter
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = users.get(position);
                Toast.makeText(Adapters.this, selectedUser.toString(), Toast.LENGTH_SHORT).show();
            }
        });

//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
//        listView.setAdapter(adapter);
//
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String selectedString = names[position];
//                Toast.makeText(Adapters.this, "POS: " + position, Toast.LENGTH_SHORT).show();
//
//
//            }
//        });
    }

    public void example2() {
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, user);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = user.get(position);
                Toast.makeText(Adapters.this, selectedUser.getFirstName(), Toast.LENGTH_SHORT).show();

            }
        });
    }

   public void example3(){
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, R.layout.custom_user_list_item,R.id.txtFirstName,user){
            @Override
            public View getView(int position, View view, ViewGroup parent){
                View itemView = super.getView(position, view, parent);

                ListView listView = (ListView)parent;
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    }
                });

                TextView lblFirstName = (TextView)itemView.findViewById(R.id.txtFirstName);
                CheckBox chkActive = (CheckBox)itemView.findViewById(R.id.chkActive);

                User u = user.get(position);
                lblFirstName.setText(u.getFirstName());
                chkActive.setChecked(u.isActive());
                //bound the user to the checkbox
                chkActive.setTag(u);
                
                chkActive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CheckBox pressedCheckBox = (CheckBox)view;
                        User u = (User)pressedCheckBox.getTag();
                        u.setActive(pressedCheckBox.isChecked());
                        Toast.makeText(Adapters.this, u.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

                return itemView;
            }
        };
        listView.setAdapter(adapter);
    }


   // UserListAdapter adapter = new UserListAdapter(this, R.layout.custom_user_list_item, R.id.lblFirstName, user);
   // listView.setAdapter(adapter);
}
