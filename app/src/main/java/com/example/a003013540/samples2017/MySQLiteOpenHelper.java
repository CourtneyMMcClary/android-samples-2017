package com.example.a003013540.samples2017;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.a003013540.samples2017.dataaccess.ItemDataAccess;

/**
 * Created by 003013540 on 11/13/2017.
 */

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String TAG = "MySQLiteHelper";
    private static final String DATA_BASE_NAME = "samples.db";
    private static final int DATABASE_VERSION = 1;

    public MySQLiteOpenHelper(Context context) {
        super(context, DATA_BASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = ItemDataAccess.TABLE_CREATE;
        db.execSQL(sql);
        Log.d(TAG, "creating db: " + sql);

        //if you need other tables for your app, you would create them here as well
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
