package com.example.a003013540.samples2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a003013540.samples2017.dataaccess.UserDataAccess;
import com.example.a003013540.samples2017.models.Item;
import com.example.a003013540.samples2017.models.User;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class UserList extends AppCompatActivity {

    AppClass app;
    ListView userListView;
    Button btnAddUser;
    UserDataAccess da;
    SQLiteHelperSubClass dbHelper;
    ArrayList<User> users;

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int buttonResourceId = view.getId();

            switch (buttonResourceId) {
                case R.id.btnAddUser:
                    startActivity(new Intent(UserList.this, UserDetailsActivity.class));
            }
        }

    };


        @Override
        protected void onCreate(Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_user_list);

            app = (AppClass) getApplication();
            //TODO: pull from db: getallusers and set app.users to array from db
            dbHelper = new SQLiteHelperSubClass(this);
            da = new UserDataAccess(dbHelper);
            app.users = da.getAllUsers();
            userListView = (ListView) findViewById(R.id.userListView);

            btnAddUser = (Button) findViewById(R.id.btnAddUser);
            btnAddUser.setOnClickListener(listener);


            ArrayAdapter adapter = new ArrayAdapter<User>(UserList.this,
                    R.layout.custom_user_list_item, R.id.txtFirstName, app.users) {

                @Override
                public View getView(int position, final View convertView, ViewGroup parent) {

                    View userListView = super.getView(position, convertView, parent);
                    //Note: listItemView will be an instance of R.layout.activity_user_details

                    User currentUser;
                    currentUser = app.users.get(position);
                   userListView.setTag(currentUser); //binding the entire User object

                    //get a handle on the widgets inside of this activity_user_details layout
                    TextView lbl = (TextView) userListView.findViewById(R.id.txtFirstName);

                    CheckBox chk = (CheckBox) userListView.findViewById(R.id.chkActive);

                    lbl.setText(currentUser.getFirstName());

                    chk.setChecked(currentUser.isActive());
                    chk.setText("Active"); //we should really use a string resource here!
                    chk.setTag(currentUser);


                    chk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            User selectedUser = (User) v.getTag();
                            selectedUser.setActive(((CheckBox) v).isChecked());
                        }
                    });

                   userListView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //v will be the custom_user_list_item that is displaying a user in the list
                            //we added a 'tag' for each one that got created, the 'tag' is storing the User
                            //object that is being displayed in v
                            User selectedUser = (User) v.getTag();
                            Intent i = new Intent(UserList.this, UserDetailsActivity.class);
                            i.putExtra(UserDetailsActivity.USER_ID_EXTRA, selectedUser.getId());
                            startActivity(i);
                        }
                    });

                    return userListView;
                }
            };

            userListView.setAdapter(adapter);
        }

    }

