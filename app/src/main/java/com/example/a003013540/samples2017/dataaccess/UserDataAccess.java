package com.example.a003013540.samples2017.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.a003013540.samples2017.MySQLiteOpenHelper;
import com.example.a003013540.samples2017.SQLiteHelperSubClass;
import com.example.a003013540.samples2017.models.User;

import java.util.ArrayList;

/**
 * Created by 003013540 on 11/15/2017.
 */

public class UserDataAccess {

    public static final String TAG = "UserDataAccess";

    SQLiteHelperSubClass dbHelper;
    SQLiteDatabase db;


    public static final String TABLE_NAME = "users";
    public static final String COLUMN_USER_ID = "_id";
    public static final String COLUMN_USER_NAME = "firstName";
    public static final String COLUMN_USER_EMAIL = "email";
    public static final String COLUMN_USER_MUSIC = "favoriteMusic";
    public static final String COLUMN_USER_ISACTIVE = "active";


    public static final String TABLE_CREATE =
            String.format("create table %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, %s TEXT)",
                    TABLE_NAME,
                    COLUMN_USER_ID,
                    COLUMN_USER_NAME,
                    COLUMN_USER_EMAIL,
                    COLUMN_USER_MUSIC,
                    COLUMN_USER_ISACTIVE
                    );


    public UserDataAccess(SQLiteHelperSubClass dbHelper){
        this.dbHelper = dbHelper;
        this.db = this.dbHelper.getWritableDatabase();
    }

    public User insertUser(User u){
        ContentValues values = new ContentValues();

        values.put(COLUMN_USER_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_USER_MUSIC, u.getFavoriteMusic().toString());
        values.put(COLUMN_USER_ISACTIVE, String.valueOf(u.isActive()));


        long insertId = db.insert(TABLE_NAME, null, values);
        //note: insertId will be -1 if the insert failed
        u.setId(insertId);
        return u;
    }

    public ArrayList<User> getAllUsers(){

        ArrayList<User> users = new ArrayList<>();
        String query = String.format("SELECT %s, %s, %s, %s, %s FROM %s", COLUMN_USER_ID, COLUMN_USER_NAME, COLUMN_USER_EMAIL, COLUMN_USER_MUSIC, COLUMN_USER_ISACTIVE, TABLE_NAME);

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        while(!c.isAfterLast()){
            User u = new User();

            u.setId(c.getLong(0));
            u.setFirstName(c.getString(1));
            u.setEmail(c.getString(2));

            String str = c.getString(3);
            User.Music um = null;

            if(str.equals("JAZZ")){
                um = User.Music.JAZZ;
            }else if(str.equals("COUNTRY")){
                um = User.Music.COUNTRY;
            }else if(str.equals("RAP")){
                um = User.Music.RAP;
            }

            u.setFavoriteMusic(um);

            String aStr = c.getString(4);
            Boolean b = null;

            if(aStr.equalsIgnoreCase("true")){
                b = true;
            }else{
                b = false;
            }

            u.setActive(b);

            users.add(u);
            c.moveToNext();
        }

        c.close();
        return users;
    }

    public User updateUser(User u){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, u.getId());
        values.put(COLUMN_USER_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_USER_MUSIC, u.getFavoriteMusic().toString());
        values.put(COLUMN_USER_ISACTIVE, Boolean.toString(u.isActive()));


        int rowsUpdated = db.update(TABLE_NAME, values, "_id = " + u.getId(), null);
        //this method returns the number of rows that were updated in the db
        //so that you could use it to confirm that your update worked

        return u;
    }

    public int deleteUser(long id){
        int rowsDeleted = db.delete(TABLE_NAME, COLUMN_USER_ID + " = " + id, null);
        //the above method returns the number of rows that were deleted
        return rowsDeleted;
    }

}
