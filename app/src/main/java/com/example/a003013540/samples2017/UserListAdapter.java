package com.example.a003013540.samples2017;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a003013540.samples2017.models.User;

import java.util.ArrayList;

import static com.example.a003013540.samples2017.AppClass.users;

/**
 * Created by 003013540 on 10/23/2017.
 */

public class UserListAdapter extends ArrayAdapter {

    private ArrayList<User> users;

    public UserListAdapter(Context context, int layoutResourceId, int x, ArrayList<User> users){
        super(context, layoutResourceId, x, users);
        this.users = users;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        View userView = super.getView(position, view, parent);
        TextView lblFirstName = (TextView)userView.findViewById(R.id.txtFirstName);
        CheckBox chkActive = (CheckBox)userView.findViewById(R.id.chkActive);

        User u = users.get(position);
        lblFirstName.setText(u.getFirstName());
        chkActive.setChecked(u.isActive());


        return userView;
    }

}
