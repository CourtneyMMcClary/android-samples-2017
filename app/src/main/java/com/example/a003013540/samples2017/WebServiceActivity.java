package com.example.a003013540.samples2017;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class WebServiceActivity extends AppCompatActivity {

    public static final String WEB_SERVICE_URL = "http://wtc-web.com/users-web-service/users/";

    EditText txtResponse;
    Button btnGetAllUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_service);

        btnGetAllUsers = (Button)findViewById(R.id.btnGetAllUsers);
        txtResponse = (EditText)findViewById(R.id.txtResponse);

        btnGetAllUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()){
                    doHttpRequest(WEB_SERVICE_URL);
                }else{
                    Toast.makeText(WebServiceActivity.this, "No Network Available", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean isNetworkAvailable(){

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        //if ni is not null and ni is connected return true; otherwise return false
        return ni != null && ni.isConnected();
    }

    private void doHttpRequest(String url){
        //making queue as soon as it can
        RequestQueue queue = Volley.newRequestQueue(this);
        //creating string request
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                //callback for volley library; taking the response the server sent back and pass it in to onResponse
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       txtResponse.setText(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(WebServiceActivity.this, "HTTP Error", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        //taking string request and adding it to volley
        queue.add(stringRequest);
    }
}
