package com.example.a003013540.samples2017;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by 003013540 on 9/18/2017.
 */

public class MainActivity extends AppCompatActivity {

    Button btnImgSpinner;
    Button btnUserDetails;
    Button btnIntentSender;
    Button btnAdapters;
    Button btnUserList;
    Button btnLifeCycle;
    Button btnCoder;
    Button btnWebService;
    Button btnSQLite;

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int buttonResourceId = view.getId();

            switch(buttonResourceId){
                case R.id.btnImgSpinner:
                    startActivity(new Intent(MainActivity.this, ImageSpinnerActivity.class));

                    break;

                case R.id.btnUserList:
                    startActivity(new Intent(MainActivity.this, UserList.class));

                    break;

                case R.id.btnIntentSender:
                    startActivity(new Intent(MainActivity.this, IntentSenderActivity.class));

                    break;

                case R.id.btnAdapters:
                    startActivity(new Intent(MainActivity.this, Adapters.class));

                    break;

                case R.id.btnLifeCycle:
                    startActivity(new Intent(MainActivity.this, LifeCycleActivity.class));

                    break;

                case R.id.btnCoder:
                    startActivity(new Intent(MainActivity.this, CoderActivity.class));

                    break;

                case R.id.btnWebService:
                    startActivity(new Intent(MainActivity.this, WebServiceActivity.class));

                    break;

                case R.id.btnSQLite:
                    startActivity(new Intent(MainActivity.this, SQLiteActivity.class));

                    break;


            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnImgSpinner = (Button)findViewById(R.id.btnImgSpinner);
        btnImgSpinner.setOnClickListener(listener);

        btnUserList = (Button)findViewById(R.id.btnUserList);
        btnUserList.setOnClickListener(listener);

        btnIntentSender = (Button)findViewById(R.id.btnIntentSender);
        btnIntentSender.setOnClickListener(listener);

        btnAdapters = (Button)findViewById(R.id.btnAdapters);
        btnAdapters.setOnClickListener(listener);

        btnLifeCycle = (Button)findViewById(R.id.btnLifeCycle);
        btnLifeCycle.setOnClickListener(listener);

        btnCoder = (Button)findViewById(R.id.btnCoder);
        btnCoder.setOnClickListener(listener);

        btnWebService = (Button)findViewById(R.id.btnWebService);
        btnWebService.setOnClickListener(listener);

        btnSQLite = (Button)findViewById(R.id.btnSQLite);
        btnSQLite.setOnClickListener(listener);

    }

}