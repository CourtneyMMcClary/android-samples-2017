package com.example.a003013540.samples2017;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.a003013540.samples2017.dataaccess.ItemDataAccess;
import com.example.a003013540.samples2017.dataaccess.UserDataAccess;

/**
 * Created by 003013540 on 11/15/2017.
 */

public class SQLiteHelperSubClass extends SQLiteOpenHelper {

    private static final String TAG = "SQLiteHelperSubClass";
    private static final String DATA_BASE_NAME = "users.db";
    private static final int DATABASE_VERSION = 1;

    public SQLiteHelperSubClass(Context context) {
        super(context, DATA_BASE_NAME, null, DATABASE_VERSION);
        Log.d(TAG, "created");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = UserDataAccess.TABLE_CREATE;
        db.execSQL(sql);
        Log.d(TAG, "creating db: " + sql);
        //if you need other tables for your app, you would create them here as well
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
