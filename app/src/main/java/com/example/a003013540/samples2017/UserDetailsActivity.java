package com.example.a003013540.samples2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.a003013540.samples2017.dataaccess.UserDataAccess;
import com.example.a003013540.samples2017.models.User;

import java.util.ArrayList;

public class UserDetailsActivity extends AppCompatActivity {

    public static final String USER_ID_EXTRA = "userid";

    AppClass app;

    User user;
    EditText txtFirstName;
    EditText txtEmail;
    RadioGroup rgFavoriteMusic;
    CheckBox chkActive;
    Button btnSave;
    Button btnClear;
    Button btnDelete;
    Button btnInject;
    UserDataAccess da;
    SQLiteHelperSubClass dbHelper;
    ArrayList<User> users;
    ArrayAdapter<User> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        //initialize appclass
        app = (AppClass) getApplication();
        dbHelper = new SQLiteHelperSubClass(this);
        da = new UserDataAccess(dbHelper);
        txtFirstName = (EditText)findViewById(R.id.txtFirstName);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        rgFavoriteMusic = (RadioGroup)findViewById(R.id.rgFavoriteMusic);
        chkActive = (CheckBox)findViewById(R.id.chkActive);
        btnSave = (Button)findViewById(R.id.btnSave);


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validation(getDataFromUI())) {
                    da.updateUser(getDataFromUI());
                    Intent i = new Intent(UserDetailsActivity.this, UserList.class);
                    startActivity(i);
                }
            }
        });

        btnClear = (Button)findViewById(R.id.btnClear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetForm();
            }
        });

        btnDelete = (Button)findViewById(R.id.btnDel);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(user != null) {
                    da.deleteUser(user.getId());
                    Intent i = new Intent(UserDetailsActivity.this, UserList.class);
                    startActivity(i);
                }
            }
        });

        btnInject = (Button)findViewById(R.id.btnInject);
        btnInject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validation(getDataFromUI())) {
                    da.insertUser(getDataFromUI());
                    Intent i = new Intent(UserDetailsActivity.this, UserList.class);
                    startActivity(i);
                    //Toast.makeText(UserDetailsActivity.this, "id" + user.getId(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Intent i = getIntent();
        long userId = i.getLongExtra(USER_ID_EXTRA, -1);
        if(userId >= 0){
            Toast.makeText(this, "GET USER: " + userId, Toast.LENGTH_SHORT).show();
            user = app.getUserById(userId);
            putDateInUI();
        }

    }

    private boolean validation(User u){
        boolean hasError = false;
        String msg = "";


        if(u.getFirstName()==null||u.getFirstName().equals("")) {
            txtFirstName.setError("Please enter first name");
            msg+="Please enter a first name.\n";
            hasError=true;
        } if(u.getEmail()==null||u.getEmail().equals("")) {
            msg+="Please enter a valid email.\n";
            hasError=true;
        } if(u.getFavoriteMusic()==null){
            msg+="Please select your favorite music.\n";
            hasError=true;
        }
        if(!hasError)
            return true;
        else{
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private User getDataFromUI(){

        String firstName = txtFirstName.getText().toString();
        String email = txtEmail.getText().toString();
        boolean active = chkActive.isChecked();
        // Here's how we can set the favorite music...
        User.Music favoriteMusic = null;
        int selectedRadioButtonId = rgFavoriteMusic.getCheckedRadioButtonId();

        switch(selectedRadioButtonId){
            case R.id.rbCountry:
                favoriteMusic = User.Music.COUNTRY;
                break;
            case R.id.rbRap:
                favoriteMusic = User.Music.RAP;
                break;
            case R.id.rbJazz:
                favoriteMusic = User.Music.JAZZ;
                break;
        }

        if(user != null){
            user.setFirstName(firstName);
            user.setEmail(email);
            user.setFavoriteMusic(favoriteMusic);
            user.setActive(active);
            return user;
        }else{
            return new User(1, firstName, email, favoriteMusic, active);
        }

    }

    private void putDateInUI(){

        if(user != null) {
            txtFirstName.setText(user.getFirstName());
            txtEmail.setText(user.getEmail());
            chkActive.setChecked(user.isActive());

            switch (user.getFavoriteMusic()) {
                case COUNTRY:
                    rgFavoriteMusic.check(R.id.rbCountry);
                    break;
                case RAP:
                    rgFavoriteMusic.check(R.id.rbRap);
                    break;
                case JAZZ:
                    rgFavoriteMusic.check(R.id.rbJazz);
            }
        }else{
            Toast.makeText(this, "hitting the else", Toast.LENGTH_SHORT).show();
        }

    }

    private void resetForm(){
        rgFavoriteMusic.clearCheck();
        txtFirstName.setText("");
        txtEmail.setText("");
        chkActive.setChecked(false);

    }

    private void refreshUsers(){
        adapter.clear();
        users = da.getAllUsers();
        adapter.addAll(users);
        adapter.notifyDataSetChanged();
    }

}